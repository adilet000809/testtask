FROM openjdk:17
ADD target/TestTask-0.0.1-SNAPSHOT.jar TestTask.jar
ENTRYPOINT ["java", "-jar", "TestTask.jar"]