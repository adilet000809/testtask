package com.example.testtask;

import com.example.testtask.dto.TransactionDto;
import com.example.testtask.dto.TransactionWithLimitDto;
import com.example.testtask.model.*;
import com.example.testtask.model.Currency;
import com.example.testtask.repository.TransactionRepository;
import com.example.testtask.service.CurrencyRateService;
import com.example.testtask.service.LimitService;
import com.example.testtask.service.impl.TransactionServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class TransactionServiceTest {

    @Mock
    private TransactionRepository transactionRepository;

    @Mock
    private LimitService limitService;

    @Mock
    private CurrencyRateService currencyRateService;

    @InjectMocks
    private TransactionServiceImpl transactionService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testFindAllLimitExceeded() {
        Date transactionDate = new Date();
        Date limitDate = new Date();
        List<TransactionWithLimitDto> expectedTransactions = Arrays.asList(
                new TransactionWithLimitDto(123456789L, 1234567890L, Currency.KZT, BigDecimal.valueOf(2000.00), ExpenseCategory.PRODUCT, transactionDate, BigDecimal.valueOf(1000), limitDate, Currency.USD),
                new TransactionWithLimitDto(111111111L, 1234567890L, Currency.KZT, BigDecimal.valueOf(2000.00), ExpenseCategory.PRODUCT, transactionDate, BigDecimal.valueOf(1000), limitDate, Currency.USD)
        );
        when(transactionRepository.findAllByLimitExceededTransactions()).thenReturn(expectedTransactions);

        List<TransactionWithLimitDto> actualTransactions = transactionService.findAllLimitExceeded();

        assertEquals(expectedTransactions.size(), actualTransactions.size());
        assertEquals(expectedTransactions, actualTransactions);
    }

    @Test
    void testLimitExceededFlagLogic() throws ParseException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS");
        List<Rate> rates = Arrays.asList(
                new Rate(1L, dateFormat.parse("2022-01-01 00:37:55.000000"), Currency.USD, Currency.KZT, null, BigDecimal.ONE),
                new Rate(2L, dateFormat.parse("2022-01-02 00:37:55.000000"), Currency.USD, Currency.KZT, BigDecimal.ONE, BigDecimal.ONE),
                new Rate(3L, dateFormat.parse("2022-01-03 00:37:55.000000"), Currency.USD, Currency.KZT, BigDecimal.ONE, BigDecimal.ONE),
                new Rate(4L, dateFormat.parse("2022-01-11 00:37:55.000000"), Currency.USD, Currency.KZT, BigDecimal.ONE, BigDecimal.ONE),
                new Rate(5L, dateFormat.parse("2022-01-12 00:37:55.000000"), Currency.USD, Currency.KZT, BigDecimal.ONE, BigDecimal.ONE),
                new Rate(5L, dateFormat.parse("2022-01-13 00:37:55.000000"), Currency.USD, Currency.KZT, BigDecimal.ONE, BigDecimal.ONE)
        );
        List<MonthlyLimit> limits = Arrays.asList(
                new MonthlyLimit(1L, 123465789L, ExpenseCategory.PRODUCT, dateFormat.parse("2022-01-01 00:00:00.000000"), BigDecimal.valueOf(1000L), Currency.USD),
                new MonthlyLimit(2L, 123465789L, ExpenseCategory.PRODUCT, dateFormat.parse("2022-01-10 00:00:11.000000"), BigDecimal.valueOf(2000L), Currency.USD)
        );

        List<TransactionDto> transactionDtoList = Arrays.asList(
                new TransactionDto(123456789L, 123L, Currency.KZT, ExpenseCategory.PRODUCT, dateFormat.parse("2022-01-02 19:41:41.000000"), BigDecimal.valueOf(500L)),
                new TransactionDto(123456789L, 123L, Currency.KZT, ExpenseCategory.PRODUCT, dateFormat.parse("2022-01-03 19:41:41.000000"), BigDecimal.valueOf(600L)),
                new TransactionDto(123456789L, 123L, Currency.KZT, ExpenseCategory.PRODUCT, dateFormat.parse("2022-01-11 19:41:41.000000"), BigDecimal.valueOf(100L)),
                new TransactionDto(123456789L, 123L, Currency.KZT, ExpenseCategory.PRODUCT, dateFormat.parse("2022-01-12 19:41:41.000000"), BigDecimal.valueOf(700L)),
                new TransactionDto(123456789L, 123L, Currency.KZT, ExpenseCategory.PRODUCT, dateFormat.parse("2022-01-13 19:41:41.000000"), BigDecimal.valueOf(100L)),
                new TransactionDto(123456789L, 123L, Currency.KZT, ExpenseCategory.PRODUCT, dateFormat.parse("2022-01-13 19:45:41.000000"), BigDecimal.valueOf(100L))
        );

        List<Transaction> expectedTransactions = Arrays.asList(
                new Transaction(null, 123456789L, 123L, Currency.KZT, ExpenseCategory.PRODUCT, BigDecimal.valueOf(500L), dateFormat.parse("2022-01-02 19:41:41.000000"), false, limits.get(0)),
                new Transaction(null, 123456789L, 123L, Currency.KZT, ExpenseCategory.PRODUCT, BigDecimal.valueOf(600L), dateFormat.parse("2022-01-03 19:41:41.000000"), true, limits.get(0)),
                new Transaction(null, 123456789L, 123L, Currency.KZT, ExpenseCategory.PRODUCT, BigDecimal.valueOf(100L), dateFormat.parse("2022-01-11 19:41:41.000000"), false, limits.get(1)),
                new Transaction(null, 123456789L, 123L, Currency.KZT, ExpenseCategory.PRODUCT, BigDecimal.valueOf(700L), dateFormat.parse("2022-01-12 19:41:41.000000"), false, limits.get(1)),
                new Transaction(null, 123456789L, 123L, Currency.KZT, ExpenseCategory.PRODUCT, BigDecimal.valueOf(700L), dateFormat.parse("2022-01-13 19:41:41.000000"), false, limits.get(1)),
                new Transaction(null, 123456789L, 123L, Currency.KZT, ExpenseCategory.PRODUCT, BigDecimal.valueOf(700L), dateFormat.parse("2022-01-13 19:45:41.000000"), true, limits.get(1))
        );


        when(limitService.findAllByAccountAndExpenseCategoryAndMonth(123456789L, ExpenseCategory.PRODUCT, 1, 2022)).thenReturn(limits);
        for (TransactionDto transactionDto: transactionDtoList) {
            when(currencyRateService.findByDateAndTargetCurrency(transactionDto.getDate(), transactionDto.getCurrencyShortName()))
                    .thenReturn(rates.stream().filter(rate -> compareDates(rate.getDate(), transactionDto.getDate())).findFirst().get());
        }

        Class<?> clazz = transactionService.getClass();

        Method method = clazz.getDeclaredMethod("processMonthlyTransactions", List.class, Long.class, ExpenseCategory.class, int.class, int.class);

        method.setAccessible(true);

        Object result = method.invoke(transactionService, transactionDtoList, 123456789L, ExpenseCategory.PRODUCT, 1, 2022);

        List<Transaction> actualTransactions = (List<Transaction>) result;

        assertEquals(expectedTransactions.size(), actualTransactions.size());
        for (int i = 0; i < expectedTransactions.size(); i++) {
            assertEquals(expectedTransactions.get(i).isLimitExceeded(), actualTransactions.get(i).isLimitExceeded());
        }

    }

    private boolean compareDates(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
                && cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH)
                && cal1.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH);
    }

}
