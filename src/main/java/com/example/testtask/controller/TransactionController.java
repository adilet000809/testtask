package com.example.testtask.controller;

import com.example.testtask.dto.LimitDto;
import com.example.testtask.dto.TransactionWithLimitDto;
import com.example.testtask.exception.EntityNotFoundException;
import com.example.testtask.model.MonthlyLimit;
import com.example.testtask.model.Transaction;
import com.example.testtask.service.LimitService;
import com.example.testtask.service.TransactionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/transactions")
@Api(tags = "Transaction API")
public class TransactionController {

    private final LimitService limitService;
    private final TransactionService transactionService;

    public TransactionController(LimitService limitService, TransactionService transactionService) {
        this.limitService = limitService;
        this.transactionService = transactionService;
    }

    @ApiOperation(value = "Receive transactions with limit_exceed flag true.", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List of transactions with limits."),
            @ApiResponse(code = 404, message = "No transactions found.")})
    @GetMapping()
    public ResponseEntity<List<TransactionWithLimitDto>> getLimitExceededTransactions() {
        try {
            List<TransactionWithLimitDto> transactions = transactionService.findAllLimitExceeded();
            return ResponseEntity.ok(transactions);
        } catch (Exception e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @ApiOperation(value = "Receive monthly limits.", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List of all monthly limits"),
            @ApiResponse(code = 404, message = "No monthly limits found")})
    @GetMapping("/limits")
    public ResponseEntity<List<MonthlyLimit>> getAllLimits() {
        try {
            List<MonthlyLimit> limits = limitService.findAllMonthlyLimits();
            return ResponseEntity.ok(limits);
        } catch (Exception e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @ApiOperation(value = "Add a new monthly limit.", consumes = "LimitDto object.", response = MonthlyLimit.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Monthly limit object created successfully.")})
    @PostMapping("/limits")
    public ResponseEntity<MonthlyLimit> addLimit(@RequestBody LimitDto limitDto) {
        MonthlyLimit monthlyLimit = limitService.save(limitDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(monthlyLimit);
    }



}
