package com.example.testtask.controller;

import com.example.testtask.dto.TransactionDto;
import com.example.testtask.service.TransactionService;
import com.example.testtask.service.impl.CurrencyRateServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/bank/")
@Api(tags = "Bank Service Integration API")
public class BankServiceController {

    private static final Logger logger = LoggerFactory.getLogger(BankServiceController.class);
    private final TransactionService transactionService;

    public BankServiceController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @ApiOperation(value = "Receive transactions from bank services", consumes = "List of TransactionDto", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Transactions received successfully."),
            @ApiResponse(code = 500, message = "Internal Server Error.")})
    @PostMapping("get")
    public ResponseEntity<String> receiveTransactions(@RequestBody List<TransactionDto> transactions) {
        try {
            logger.info("Transactions received.");
            transactionService.save(transactions);
            return ResponseEntity.ok("Transactions received successfully.");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error processing transactions: " + e.getMessage());
        }
    }

}
