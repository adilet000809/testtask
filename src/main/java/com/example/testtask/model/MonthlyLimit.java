package com.example.testtask.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MonthlyLimit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "account")
    private Long account;

    @Enumerated(EnumType.STRING)
    @Column(name = "expense_category")
    private ExpenseCategory expenseCategory;

    @Basic
    @Column(name = "date")
    private Date date;

    @Column(name = "limit_sum")
    private BigDecimal limitSum;

    @Enumerated(EnumType.STRING)
    @Column(name = "limit_currency_shortname")
    private Currency limitCurrencyShortName;

}
