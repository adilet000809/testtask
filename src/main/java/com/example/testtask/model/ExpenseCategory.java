package com.example.testtask.model;

public enum ExpenseCategory {
    PRODUCT, SERVICE
}
