package com.example.testtask.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "account_from")
    private Long accountFrom;

    @Column(name = "account_to")
    private Long accountTo;

    @Enumerated(EnumType.STRING)
    @Column(name = "currency_shortname")
    private Currency currencyShortName;

    @Enumerated(EnumType.STRING)
    @Column(name = "expense_category")
    private ExpenseCategory expenseCategory;

    @Column(name = "sum")
    private BigDecimal sum;

    @Column(name = "date")
    private Date date;

    @Column(name = "limit_exceeded")
    private boolean limitExceeded;

    @ManyToOne
    @JoinColumn(name = "monthly_limit_id")
    private MonthlyLimit monthlyLimit;

}
