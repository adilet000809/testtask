package com.example.testtask.dto;

import com.example.testtask.model.Currency;
import com.example.testtask.model.ExpenseCategory;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@AllArgsConstructor
public class TransactionDto {

    private Long accountFrom;
    private Long accountTo;
    private Currency currencyShortName;
    private ExpenseCategory expenseCategory;
    private Date date;
    private BigDecimal sum;

}
