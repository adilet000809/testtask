package com.example.testtask.dto;

import com.example.testtask.model.Currency;
import com.example.testtask.model.ExpenseCategory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;


@AllArgsConstructor
@Getter
@Setter
public class TransactionWithLimitDto {

    private Long accountFrom;
    private Long accountTo;
    private Currency currencyShortName;
    private BigDecimal sum;
    private ExpenseCategory expenseCategory;
    private Date date;
    private BigDecimal limitSum;
    private Date limitDate;
    private Currency limitCurrencyShortName;

}
