package com.example.testtask.dto;

import com.example.testtask.model.ExpenseCategory;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class LimitDto {

    private Long account;
    private ExpenseCategory expenseCategory;
    private Date date;
    private BigDecimal limitSum = BigDecimal.valueOf(1000);


}
