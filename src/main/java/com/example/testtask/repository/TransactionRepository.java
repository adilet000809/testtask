package com.example.testtask.repository;

import com.example.testtask.dto.TransactionWithLimitDto;
import com.example.testtask.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query("SELECT " +
            "new com.example.testtask.dto.TransactionWithLimitDto(" +
            "t.accountFrom, t.accountTo, t.currencyShortName, " +
            "t.sum, t.expenseCategory, t.date, " +
            "l.limitSum, l.date, l.limitCurrencyShortName) " +
            "FROM Transaction t " +
            "JOIN t.monthlyLimit l " +
            "WHERE t.limitExceeded=true")
    List<TransactionWithLimitDto> findAllByLimitExceededTransactions();

}
