package com.example.testtask.repository;

import com.example.testtask.model.ExpenseCategory;
import com.example.testtask.model.MonthlyLimit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface LimitRepository extends JpaRepository<MonthlyLimit, Long> {

    @Query("SELECT ml FROM MonthlyLimit ml " +
            "WHERE ml.account = :account " +
            "AND ml.expenseCategory = :expenseCategory " +
            "AND EXTRACT(MONTH FROM ml.date) = :month " +
            "AND EXTRACT(YEAR FROM ml.date) = :year " +
            "ORDER BY ml.date ASC")
    List<MonthlyLimit> findAllByAccountAndExpenseCategoryAndMonthAndYear(@Param("account") Long account, @Param("expenseCategory") ExpenseCategory expenseCategory, @Param("month") int month, @Param("year") int year);

    @Query("SELECT DISTINCT m.account FROM MonthlyLimit m")
    List<Long> findAllAccountNumbersOfExistingMonthlyLimits();
}
