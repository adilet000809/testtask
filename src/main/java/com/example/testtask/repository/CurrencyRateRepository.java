package com.example.testtask.repository;

import com.example.testtask.model.Currency;
import com.example.testtask.model.Rate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface CurrencyRateRepository extends JpaRepository<Rate, Long> {

    @Query("SELECT r FROM Rate r WHERE DATE(r.date) = DATE(:date) AND r.targetCurrency=:targetCurrency")
    Optional<Rate> findByDateAndTargetCurrency(Date date, Currency targetCurrency);

}
