package com.example.testtask.service.impl;

import com.example.testtask.exception.EntityNotFoundException;
import com.example.testtask.model.Currency;
import com.example.testtask.model.Rate;
import com.example.testtask.repository.CurrencyRateRepository;
import com.example.testtask.service.CurrencyRateService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class CurrencyRateServiceImpl implements CurrencyRateService {

    private static final Logger logger = LoggerFactory.getLogger(CurrencyRateServiceImpl.class);

    private final CurrencyRateRepository repository;
    private final RestTemplate restTemplate;
    private final ThreadPoolTaskScheduler threadPoolTaskScheduler;
    private final CronTrigger cronTrigger;

    public CurrencyRateServiceImpl(CurrencyRateRepository repository, RestTemplate restTemplate,
                                   ThreadPoolTaskScheduler threadPoolTaskScheduler,
                                   @Qualifier("rateFetchSchedule") CronTrigger cronTrigger) {
        this.repository = repository;
        this.restTemplate = restTemplate;
        this.threadPoolTaskScheduler = threadPoolTaskScheduler;
        this.cronTrigger = cronTrigger;
    }

    @PostConstruct
    private void fetchDailyCurrencyRates() {
        threadPoolTaskScheduler.schedule(new CurrencyRateServiceImpl.CurrencyRatesFetcher(), cronTrigger);
    }

    @Override
    public Rate findByDateAndTargetCurrency(Date date, Currency targetCurrency) {
        Optional<Rate> rate = repository.findByDateAndTargetCurrency(date, targetCurrency);
        return rate.orElseThrow(() -> new EntityNotFoundException(String.format("Currency rates for %s not found.", date)));
    }

    private class CurrencyRatesFetcher implements Runnable {

        private static final String URL = "https://api.twelvedata.com/time_series/";
        private static final String API_KEY = "20c0b153110d4d8099a8221db2036281";
        private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

        @Override
        public void run() {
            saveCurrencyRateDaily();
        }

        private void saveCurrencyRateDaily() {
            List<Rate> fetchedRates = fetchCurrencyRates();
            List<Rate> rateToAdd = new ArrayList<>();
            if (fetchedRates.isEmpty()) {
                logger.warn(String.format("Currency rates for: %s couldn't be fetched.", new Date()));
                return;
            }
            for (Rate rate: fetchedRates) {
                Rate todayRate = getTodayRate(rate);
                rateToAdd.add(todayRate);
            }
            repository.saveAll(rateToAdd);
            logger.info(String.format("Currency rates for: %s saved into DB.", new Date()));
        }

        private List<Rate> fetchCurrencyRates() {
            logger.info(String.format("Calling API to fetch currency rates for: %s", new Date()));
            UriComponentsBuilder urlBuilder = UriComponentsBuilder.fromHttpUrl(URL)
                    .queryParam("apikey", API_KEY)
                    .queryParam("interval", "1day")
                    .queryParam("outputsize", 1)
                    .queryParam("symbol", "USD/KZT,USD/RUB")
                    .queryParam("dp", 2)
                    .queryParam("previous_close", true);
            ResponseEntity<String> response = restTemplate.getForEntity(urlBuilder.toUriString(), String.class);
            if (response.getStatusCode().is4xxClientError()) {
                logger.info(String.format("Error occured while fetching currency rates for: %s", new Date()));
                return Collections.emptyList();
            }
            return parseCurrencyRatesFromResponse(response.getBody());
        }

        private List<Rate> parseCurrencyRatesFromResponse(String json) {
            List<Rate> rates = new ArrayList<>();
            ObjectMapper mapper = new ObjectMapper();

            try {
                JsonNode root = mapper.readTree(json);
                Iterator<String> symbolIterator = root.fieldNames();
                while (symbolIterator.hasNext()) {
                    String symbol = symbolIterator.next();
                    JsonNode currencyNode = root.get(symbol);
                    JsonNode currencyData = currencyNode.get("values").get(0);

                    String datetime = currencyData.get("datetime").asText();
                    double close = currencyData.get("close").asDouble();
                    double previousClose = currencyData.get("previous_close").asDouble();

                    Rate rate = new Rate();
                    rate.setBaseCurrency(Currency.valueOf(symbol.split("/")[0]));
                    rate.setTargetCurrency(Currency.valueOf(symbol.split("/")[1]));
                    rate.setDate(DATE_FORMAT.parse(datetime));
                    rate.setClose(BigDecimal.valueOf(close));
                    rate.setPreviousClose(BigDecimal.valueOf(previousClose));

                    rates.add(rate);
                }
                return rates;
            } catch (JsonProcessingException e) {
                logger.error("Error parsing JSON {}:", e.getMessage());
                return Collections.emptyList();
            } catch (ParseException e) {
                logger.error("Error parsing Date {}:", e.getMessage());
                return Collections.emptyList();
            }

        }

        private static Rate getTodayRate(Rate rate) {
            Rate todayRate = new Rate();
            if (rate.getDate().before(new Date())) {
                todayRate.setBaseCurrency(rate.getBaseCurrency());
                todayRate.setTargetCurrency(rate.getTargetCurrency());
                todayRate.setDate(new Date());
                todayRate.setClose(null);
                todayRate.setPreviousClose(rate.getClose());
            } else {
                todayRate.setBaseCurrency(rate.getBaseCurrency());
                todayRate.setTargetCurrency(rate.getTargetCurrency());
                todayRate.setDate(rate.getDate());
                todayRate.setClose(rate.getClose());
                todayRate.setPreviousClose(rate.getPreviousClose());
            }
            return todayRate;
        }

    }

}
