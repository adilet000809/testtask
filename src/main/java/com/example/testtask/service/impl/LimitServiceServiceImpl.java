package com.example.testtask.service.impl;

import com.example.testtask.dto.LimitDto;
import com.example.testtask.model.Currency;
import com.example.testtask.model.ExpenseCategory;
import com.example.testtask.model.MonthlyLimit;
import com.example.testtask.repository.LimitRepository;
import com.example.testtask.service.LimitService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class LimitServiceServiceImpl implements LimitService {

    private static final Logger logger = LoggerFactory.getLogger(LimitServiceServiceImpl.class);
    private final LimitRepository limitRepository;
    private final ThreadPoolTaskScheduler threadPoolTaskScheduler;
    private final CronTrigger cronTrigger;

    public LimitServiceServiceImpl(LimitRepository limitRepository, ThreadPoolTaskScheduler threadPoolTaskScheduler, @Qualifier("limitSetSchedule") CronTrigger cronTrigger) {
        this.limitRepository = limitRepository;
        this.threadPoolTaskScheduler = threadPoolTaskScheduler;
        this.cronTrigger = cronTrigger;
    }

    @PostConstruct
    private void setMonthlyLimits() {
        threadPoolTaskScheduler.schedule(new LimitServiceServiceImpl.LimitSetter(), cronTrigger);
    }

    @Override
    public MonthlyLimit save(LimitDto limitDto) {
        MonthlyLimit monthlyLimit = new MonthlyLimit();
        monthlyLimit.setAccount(limitDto.getAccount());
        monthlyLimit.setDate(new Date());
        monthlyLimit.setLimitSum(limitDto.getLimitSum());
        monthlyLimit.setExpenseCategory(limitDto.getExpenseCategory());
        monthlyLimit.setLimitCurrencyShortName(Currency.USD);
        return limitRepository.save(monthlyLimit);
    }

    @Override
    public List<MonthlyLimit> findAllByAccountAndExpenseCategoryAndMonth(Long account, ExpenseCategory expenseCategory, int month, int year) {
        return limitRepository.findAllByAccountAndExpenseCategoryAndMonthAndYear(account, expenseCategory, month, year);
    }

    @Override
    public List<MonthlyLimit> findAllMonthlyLimits() {
        return limitRepository.findAll();
    }

    private static List<MonthlyLimit> getMonthlyLimits(List<Long> accountNumbers, ExpenseCategory expenseCategory) {
        List<MonthlyLimit> monthlyLimits = new ArrayList<>();
        accountNumbers.forEach(accountNumber -> {
            MonthlyLimit newMonthlyLimit = new MonthlyLimit();
            newMonthlyLimit.setAccount(accountNumber);
            newMonthlyLimit.setDate(new Date());
            newMonthlyLimit.setLimitSum(BigDecimal.valueOf(1000));
            newMonthlyLimit.setLimitCurrencyShortName(Currency.USD);
            newMonthlyLimit.setExpenseCategory(expenseCategory);
        });
        return monthlyLimits;
    }

    private class LimitSetter implements Runnable {

        @Override
        public void run() {
            setMonthlyLimits();
        }

        private void setMonthlyLimits() {
            logger.info("Set new 1000 USD monthly limit for each account");
            List<Long> accountNumbers = limitRepository.findAllAccountNumbersOfExistingMonthlyLimits();
            List<MonthlyLimit> monthlyLimits = new ArrayList<>();
            monthlyLimits.addAll(getMonthlyLimits(accountNumbers, ExpenseCategory.PRODUCT));
            monthlyLimits.addAll(getMonthlyLimits(accountNumbers, ExpenseCategory.SERVICE));
            limitRepository.saveAll(monthlyLimits);
        }


    }



}
