package com.example.testtask.service.impl;

import com.example.testtask.dto.TransactionDto;
import com.example.testtask.dto.TransactionWithLimitDto;
import com.example.testtask.exception.EntityNotFoundException;
import com.example.testtask.model.*;
import com.example.testtask.model.Currency;
import com.example.testtask.repository.TransactionRepository;
import com.example.testtask.service.CurrencyRateService;
import com.example.testtask.service.LimitService;
import com.example.testtask.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TransactionServiceImpl implements TransactionService {

    private static final Logger logger = LoggerFactory.getLogger(TransactionServiceImpl.class);
    private final TransactionRepository transactionRepository;
    private final LimitService limitService;
    private final CurrencyRateService currencyRateService;

    public TransactionServiceImpl(TransactionRepository transactionRepository, LimitService limitService, CurrencyRateService currencyRateService) {
        this.transactionRepository = transactionRepository;
        this.limitService = limitService;
        this.currencyRateService = currencyRateService;
    }

    @Override
    public List<TransactionWithLimitDto> findAllLimitExceeded() {
        return transactionRepository.findAllByLimitExceededTransactions();
    }

    @Override
    public List<Transaction> save(List<TransactionDto> transactionDtoList) {
        List<Transaction> transactions = processTransactions(transactionDtoList);
        return transactionRepository.saveAll(transactions);
    }

    private List<Transaction> processTransactions(List<TransactionDto> transactionDtoList) {
        List<Transaction> transactions = new ArrayList<>();
        //Grouping transactions by account number
        Map<Long, List<TransactionDto>> groupedByAccountTransactions = transactionDtoList.stream()
                .collect(Collectors.groupingBy(TransactionDto::getAccountFrom));

        groupedByAccountTransactions.forEach((account, transactionList) -> transactions.addAll(process(account, transactionList)));
        return transactions;
    }

    private List<Transaction> process(Long account, List<TransactionDto> transactions) {
        List<Transaction> monthlyTransactions = new ArrayList<>();

        //Transaction made for PRODUCT
        List<TransactionDto> productTransactions = transactions.stream()
                .filter(t -> t.getExpenseCategory().equals(ExpenseCategory.PRODUCT))
                .sorted(Comparator.comparing(TransactionDto::getDate))
                .toList();
        //Transaction made for SERVICE
        List<TransactionDto> serviceTransactions = transactions.stream()
                .filter(t -> t.getExpenseCategory().equals(ExpenseCategory.SERVICE))
                .sorted(Comparator.comparing(TransactionDto::getDate))
                .toList();

        //Grouping transactions by month and year
        Map<String, List<TransactionDto>> productTransactionsByMonthAndYear = productTransactions.stream()
                .collect(Collectors.groupingBy(
                        transaction -> getMonthAndYear(transaction.getDate())
                ));

        //Grouping transactions by month and year
        Map<String, List<TransactionDto>> serviceTransactionsByMonthAndYear = serviceTransactions.stream()
                .collect(Collectors.groupingBy(
                        transaction -> getMonthAndYear(transaction.getDate())
                ));

        productTransactionsByMonthAndYear.forEach((monthAndYear, transactionDtoByMonth) -> {
            int month = Integer.parseInt(monthAndYear.split("-")[0]);
            int year = Integer.parseInt(monthAndYear.split("-")[1]);
            monthlyTransactions.addAll(processMonthlyTransactions(transactionDtoByMonth, account, ExpenseCategory.PRODUCT, month, year));
        });

        serviceTransactionsByMonthAndYear.forEach((monthAndYear, transactionDtoByMonth) -> {
            int month = Integer.parseInt(monthAndYear.split("-")[0]);
            int year = Integer.parseInt(monthAndYear.split("-")[1]);
            monthlyTransactions.addAll(processMonthlyTransactions(transactionDtoByMonth, account, ExpenseCategory.SERVICE, month, year));
        });

        return monthlyTransactions;

    }

    private List<Transaction> processMonthlyTransactions(List<TransactionDto> transactions, Long account, ExpenseCategory expenseCategory, int month, int year) {
        List<Transaction> monthlyTransactions = new ArrayList<>();
        List<MonthlyLimit> limits = limitService.findAllByAccountAndExpenseCategoryAndMonth(account, expenseCategory, month, year);


        MonthlyLimit currentLimit;
        BigDecimal totalLimit;
        BigDecimal totalSum = BigDecimal.ZERO;
        for (TransactionDto transactionDto: transactions) {
            Date transactionDate = transactionDto.getDate();

            currentLimit = limits.stream()
                    .filter(l -> l.getDate().before(transactionDto.getDate()))
                    .reduce((first, second) -> second)
                    .orElseThrow(() -> new EntityNotFoundException(String.format("Monthly limit for a transaction made on: %s not found", transactionDate)));

            totalLimit = currentLimit.getLimitSum().subtract(totalSum);

            Currency currencyOfTransaction = transactionDto.getCurrencyShortName();
            Rate rate = currencyRateService.findByDateAndTargetCurrency(transactionDate, currencyOfTransaction);
            BigDecimal priceOfUSD = rate.getClose() == null ? rate.getPreviousClose(): rate.getClose();

            BigDecimal transactionSum = transactionDto.getSum();
            BigDecimal transactionSumInUSD = transactionSum.divide(priceOfUSD, 2, RoundingMode.HALF_UP);

            totalSum = totalSum.add(transactionSumInUSD);
            totalLimit = totalLimit.subtract(transactionSumInUSD);

            Transaction transaction = getTransaction(transactionDto, totalLimit, transactionSumInUSD, currentLimit);
            monthlyTransactions.add(transaction);
        }
        logger.info(String.format("Tranactions made on %d-%d have been proccessed.", month, year));
        return monthlyTransactions;
    }

    private static Transaction getTransaction(TransactionDto transactionDto, BigDecimal totalLimit, BigDecimal transactionSumInUSD, MonthlyLimit currentLimit) {
        Transaction transaction = new Transaction();
        transaction.setAccountFrom(transactionDto.getAccountFrom());
        transaction.setAccountTo(transactionDto.getAccountTo());
        transaction.setDate(transactionDto.getDate());
        transaction.setCurrencyShortName(transactionDto.getCurrencyShortName());
        transaction.setExpenseCategory(transactionDto.getExpenseCategory());
        transaction.setLimitExceeded(totalLimit.compareTo(BigDecimal.ZERO) < 0);
        transaction.setSum(transactionSumInUSD);
        transaction.setMonthlyLimit(currentLimit);
        return transaction;
    }

    private static String getMonthAndYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);
        return month + "-" + year;
    }

}
