package com.example.testtask.service;

import com.example.testtask.dto.TransactionDto;
import com.example.testtask.dto.TransactionWithLimitDto;
import com.example.testtask.model.Transaction;

import java.util.List;

public interface TransactionService {

    List<TransactionWithLimitDto> findAllLimitExceeded();
    List<Transaction> save(List<TransactionDto> transactions);

}
