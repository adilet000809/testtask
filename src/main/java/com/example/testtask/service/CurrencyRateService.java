package com.example.testtask.service;

import com.example.testtask.model.Currency;
import com.example.testtask.model.Rate;

import java.util.Date;
import java.util.List;

public interface CurrencyRateService {

    Rate findByDateAndTargetCurrency(Date date, Currency targetCurrency);

}
