package com.example.testtask.service;

import com.example.testtask.dto.LimitDto;
import com.example.testtask.model.ExpenseCategory;
import com.example.testtask.model.MonthlyLimit;

import java.util.List;

public interface LimitService {

    MonthlyLimit save(LimitDto limitDto);
    List<MonthlyLimit> findAllByAccountAndExpenseCategoryAndMonth(Long account, ExpenseCategory expenseCategory, int month, int year);
    List<MonthlyLimit> findAllMonthlyLimits();

}
