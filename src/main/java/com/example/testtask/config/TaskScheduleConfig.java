package com.example.testtask.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;

@Configuration
public class TaskScheduleConfig {

    @Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
        threadPoolTaskScheduler.setPoolSize(5);
        threadPoolTaskScheduler.setThreadNamePrefix("ScheduledTask");
        return threadPoolTaskScheduler;
    }

    @Bean(name = "limitSetSchedule")
    public CronTrigger cronTriggerForLimit() {
        return new CronTrigger("0 0 0 1 * ?");
    }

    @Bean(name = "rateFetchSchedule")
    public CronTrigger cronTriggerForRate() {
        return new CronTrigger("0 0 0 * * *");
    }


}
