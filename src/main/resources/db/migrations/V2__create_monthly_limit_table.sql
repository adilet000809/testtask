CREATE TABLE monthly_limit (
   id SERIAL PRIMARY KEY,
   account BIGINT NOT NULL,
   expense_category VARCHAR(255) NOT NULL,
   date TIMESTAMP WITH TIME ZONE NOT NULL,
   limit_sum NUMERIC,
   limit_currency_shortname VARCHAR(3)
);