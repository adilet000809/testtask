CREATE TABLE transaction (
     id SERIAL PRIMARY KEY,
     account_from BIGINT NOT NULL,
     account_to BIGINT NOT NULL,
     currency_shortname VARCHAR(3) NOT NULL,
     expense_category VARCHAR(255) NOT NULL,
     sum NUMERIC,
     date TIMESTAMP WITH TIME ZONE NOT NULL,
     limit_exceeded BOOLEAN NOT NULL,
     monthly_limit_id BIGINT,
     FOREIGN KEY (monthly_limit_id) REFERENCES monthly_limit(id)
);