INSERT INTO monthly_limit (account, expense_category, date, limit_sum, limit_currency_shortname)
VALUES
    (123456789, 'PRODUCT', '2022-01-01 00:00:00', 1000.00, 'USD'),
    (123456789, 'PRODUCT', '2022-01-10 00:00:00', 2000.00, 'USD'),
    (123456789, 'SERVICE', '2022-01-01 00:00:00', 1000.00, 'USD'),
    (123456789, 'SERVICE', '2022-01-10 00:00:00', 400.00, 'USD')
;